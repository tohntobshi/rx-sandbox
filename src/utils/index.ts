import { Observable, Subscription, of, from, Observer, timer } from 'rxjs';

export function getRelativeCoordinates(event: any): { x: number; y: number; } {
  return {
    x: event.clientX,
    y: event.clientY,
  }
}

export function observableGenerator(generator: GeneratorFunction, ...args: any[]): Observable<any> {
  return new Observable((observer: Observer<any>) => {
    try {
      var iterator: Generator = generator(...args);
    } catch (e) {
      observer.error(e);
    }
    let currentSubscription: Subscription;
    let currentValue: Observable<any> = timer(0);
    let isDone: boolean = false;
    function checkComletion() {
      if (isDone && (!currentValue || !currentValue.subscribe)) {
        observer.next(currentValue);
        observer.complete();
        return true;
      }
    }
    function onNext(val: any) {
      try {
        currentSubscription.unsubscribe();
        ({ value: currentValue, done: isDone } = iterator.next(val));
        if (checkComletion()) return;
        currentSubscription = currentValue.subscribe({
          next: onNext,
          error: onError
        });
      } catch (e) {
        onError(e);
      }
    }
    // sending errors back to generator
    function onError(err: Error) {
      try {
        ({ value: currentValue, done: isDone } = iterator.throw(err));
        if (checkComletion()) return;
        currentSubscription = currentValue.subscribe({
          next: onNext,
          error: onError
        });
      } catch (e) {
        observer.error(e);
      }
    }
    // start first iteration
    currentSubscription = currentValue.subscribe(onNext);
    // teardown logic
    return () => currentSubscription.unsubscribe();
  });
}
