import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
const styles = require('./styles.scss');
import { DraggableBlock } from ':components/DraggableBlock';
import { GlobalState } from ':types';
import { ActionCreators } from ":actions";


interface Props {
  x?: number;
  y?: number;
  ping: () => void;
}


export const StartPing = ({ x, y, ping}: Props) => (
  <DraggableBlock x={x} y={y}>
    <div className={styles.container}>
      <button onClick={ping}>Ping</button>
    </div>
  </DraggableBlock>
);


const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  ping: ActionCreators.ping
}, dispatch);

export default connect(null, mapDispatchToProps)(StartPing);
