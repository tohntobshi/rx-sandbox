import { Actions, ActionCreators } from ':actions';
import { Observable, of, never } from 'rxjs';
import { pluck, debounceTime, distinctUntilChanged, switchMap, tap, takeUntil, catchError } from 'rxjs/operators';
import { GlobalState } from ':types';
import { StateObservable, ofType } from 'redux-observable';
import { Action } from 'redux';
import { observableGenerator } from ':utils';



export const startPing = (action$: Observable<Action>, store$: StateObservable<GlobalState>): Observable<Action> => {
  return action$.pipe(
    ofType(Actions.PING),
    switchMap(() => observableGenerator(<GeneratorFunction><unknown>function* () {
      console.log('ping');
      const val = yield new Observable((observer) => { setTimeout(() =>  observer.next('ololo'), 1000); });
      console.log(val);
      console.log('after ololo');
      const val2 = yield of('immediate value');
      console.log(val2);
      const val3 = yield new Observable((observer) => { setTimeout(() =>  observer.next('ololo3'), 3000); });
      console.log(val3);
      let val4;
      // try {
      //   val4 = yield new Observable((observer) => { setTimeout(() =>  observer.error('my error'), 3000); });
      // } catch (e) {
      //   console.log('caught', e);
      // }
      const val5 = yield new Observable((observer) => { setTimeout(() =>  observer.next('ololo5'), 3000); });
      console.log(val5);
      try {
        yield 'some shit';
      } catch(e) {
        console.log('tried to yield some shit', e);
      }
      throw new Error('whatever');
      return ActionCreators.pong();
    }).pipe(
      catchError((e) => {
        console.log('caugth observable error', e);
        return of();
      })
      // takeUntil(new Observable((observer) => { setTimeout(() =>  observer.next('cancel'), 3000); }))
    )),
    tap(action => console.log('Action dispatched:', action))
  );
}