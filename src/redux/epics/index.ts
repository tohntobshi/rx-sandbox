import { combineEpics } from 'redux-observable';
import { saveText } from './textToSave';
import { logIn, logOut } from './session';
import { startPing } from './ping';

export default combineEpics(
  saveText,
  logIn,
  logOut,
  startPing
);